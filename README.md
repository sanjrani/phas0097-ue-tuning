# PHAS0097 UE Tuning

This is my UCL MSci Physics project on tuning LEP and HERA data using Pythia. Supervised by Prof Matthew Wing, and Prof Jon Butterworth

***

# Project Status and Updates
### Date: 23 Jan 2022
Hello, as stated above, this is my UCL MSci Physics project. Admittedly, I have been a little inconsistent with writing weekly update files. That will change now (as of 24 Jan '22, when things really get kicking) as up until recently, I have just been struggling with the setup.

I have been working on trying to sort out the pythia/rivet interface, as on the UCL HEP cluster, it seems to not be working as intended. The issue being that Pythia doesn't seem to be accessing the correct Rivet installation. This is discussed more in the associated file where I will try and describe the trouble shooting for this. 

On top of this, I successfully wrote some python scripts which can a) submit jobs to the batch farm, and b) read the .dat files and produce some rudimentary plots.

The current focus from last meeting (20 Jan 22) has been for this:

- MPI off and on for all analyses good start (working ok on MC and what’s sensitive to it)
- Fix EP
- PT0ref parameter scan (related to number of scatters)
- Cut efficiency study (detailed)

***

## Project Description

This project is on understanding the underlying event in the photoproduction regime of EP collisions, in which the photon acts as if it has partons, thus suggesting beam remnants and multiparton interactions may take place, just like in hadronic collisions. 

I am to achieve this through a tuning of the particle-level Pythia Monte Carlo (MC) generator as it is the only one capable of photoproduction at EP colliders currently. 

This is achieved by generating events through Pythia, and using the Rivet interface, which is an analysis toolkit that hosts the code associated with published analyses from different collider experiments. This interface allows the generated MC events from Pythia to follow the same analyses of the original data, and a direct comparison can be made through the Rivet plots outputted. 

This is to be done with HERA analyses and LEP analyses, the papers of which are in the Analysis_PDFs directory, as HERA has not been well studied for this whereas LEP has. Hence, a comparison of how the MPI model acts might differ. I am not sure if beam remnants will be studied, it depends on the progress made in this project.

## Technical issues
The main technical issues that have arisen have been:

- Difficulty in working with Professor for automated tuning
- Issues on the cluster
- Issues of installation on local computer
- EP event generation not completing because of a Rivet error
- Fixing the above error has also been difficult

These have been resolved, respectively, by:

- Moving to manual tuning to understand the tuning process
- Maintenence issue was fixed
- Used docker pythia-rivet interface. Although this made it difficult to a) combine with Professor and bash scripting and b) to edit the Rivet files to solve the EP generation error
- `DISKinematics.cc` file has an `assert` at the end that transforms the kinematics into the breit frame, which needs to be commented out
- This last one has not yet been resolved as we are not sure about why the modified Rivet installation is ignored by Pythia, as this has also shown through adding a new analysis (ZEUS_2021) which isn't usable because of this


## Installation and Event Generation
Pythia is a relatively simple installation, as detailed on their website. 

1. Download and extract the tar ball
2. Configure what external libraries you want to include (we need rivet so `./configure --with-rivet`
3. `make`

Then, we have been using the example file, `main93` which can be used to interface with Rivet. All that needs to be done for running this is to use:
1. `cd examples/` and `make main93` to make it executable
2. create run card, or modify the existing one (examples given in run card directory)
4. run `./main93 -c <runcard_name>.cmnd -n <number of events> -o <output_filename>
5. wait
6. should produce a `.log` and `.yoda` file, can check these out if you'd like
7. run `rivet-mkhtml <output_filename>.yoda` to produce the Rivet plots

Code written so far has really been to automate the steps in the above list for multiple different parameters. This needs to be done so we can analyse multiple plots at once!


## Sections to be added to this:

## Python scripts description

This is a general description of how the python scripts work:

### Generating Runs

There is a batch farm script associated with each analysis, in which I specify what I want to do: number of repeats (using 100k events each time) which produces a different seed (increase by 1 each time), and what parameter scape I want to vary. This works by:

1.  having a template script for each analysis (i.e., the beam energies, and the associated settings that are meant to always be there) and a section for parameters and phase cuts specified by `! Parameters` and `! Phase Cuts`, respectively.

2. read the template script, find the Parameters and Phase Cut sections, then add the associated parameters and phase cuts specified in the arguments. This is the function `run_card_initializer(analysis_template_name, phase_cut, parameters, run_card_name)` and creates the run card for a particular parameter/phase cut set

3. modify this run card with `run_card_repeater(settings_set_name, run_card_name, n_repeats)` which takes the name you want to call that settings set, then the run card name you chose to produce the run card with, and an amount of repeats you want.
    - the `run_card_repeater` function creates run card file names `run_card_name + str(n)` where n is the repeat in the number of repeats and adds the incremental seed number to it to ensure none of the same data is made
    - the `run_repeater(settings_set_name, run_card_name, n_repeats)` function is called
        - this calls the `batch_submit_script` function that returns the name of a script it generated for submitting a batch job to the cluster 
        - the list of batch farm script names is given to `batch_submit_qsub` that creates a bash script that submits all of these jobs at once so they can be done in parallel
        
4. The bash script from `batch_submit_qsub` then needs to be made executable with `chmod +x <batch_submit_qsub_name>` and then run `./<batch_submit_qsub_name>`

This code probably has room for more efficiency, which I am welcome to implement if given the idea. 

### Reading the output

Associated with the output of that particular settings run are two python scripts, which, I suppose could be converted to one:

**Plotter.py**
1. a function `dat_numbers_collector(dat_file_name)` to go through a .dat file and read the tables by recognising where the beginning and end of the tables are --> returns a list of lists (list of rows, that have been split by column)
2. a function `dat_file_reformatter(table)` returns a `pandas` dataframe of each table you feed in from the above function
3. `averager(dat_file_name)`, takes the `dat_file_name` we want to analyse, and goes through all of the directories in the output file associated with each repeat, then gets the data from all of the files that have that .dat file name, and averages each bin of the MC outputs
4. `plotter(dat_file_name)` that plots the averages of all the repeats

**rivet_to_html.py**
Remembering that the output (`.yoda` and `.log` files) are all grouped in a directory, this script produces a bash script to be `chmod +x <bash_script>` and `./<bash_script>` that contains the lines to make each individual `.yoda` file turn into its html output through the `rivet-mkhtml` command, with the names `<output_file_name>_plots` 


## Physics results

Here lies the most important updates and findings from the below sections

### Plot consistency checks

### Cut efficiency

### Tuning
