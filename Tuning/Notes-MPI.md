# PHAS0097 UE Tuning - Tuning MPI notes
Date: 19.02.22

This little document is on the thought process and notes and observations made while I've been looking at the MPI parameters in the PYTHIA8 model for HERA data.

I'll start off with describing the situation and context of it all first, then I'll get into the nitty gritty, and then my approach for it. 

## The situation in PYTHIA

Currently, only have tuned parameters from proton-(anti)proton data, and LEP data. Each of these have produced different results. This is expected considering the different CMS energies and regimes whereby the resolved photon structure is less hadron-like than that of a proton with less well developed PDFs resulting in less MPI's.

So much so, that the photon-photon MPI model has separate parameters that override the default MPI ones. (Need to ask Ilkka for the reference to this study.)

On top of this, the addition of resolved photoproduction into PYTHIA has made it available to look at MPI, this has been looked at already in 1., in which it was found that the key MPI parameter `pT0ref` has been tuned to 3.30? (although, the default in pythia for photon-photon is set to 1.52? I am confused about this).

## Description of MPI and its parameters

As specified in [1], the probability for MPIs in PYTHIA8 is given by 2->2 QCD processes


## Runs

### Defaults and MPI on / off

Calculate chi squared for each of all the distributions here? 

Just for some checks to see things are working:

- `OPAL_2008_MPI_onoff`: with 10 million events, at `pthatmin = 1 GeV, q2max = 2 GeV2, Wmin = 5 GeV` (although, this was done prior to being aware of `photoncollision=all` not being accurate) 

- `ZEUS_2001_CE_MPIonoff1`: with 10 million events, at `pthatmin = 6 GeV, q2max = 1 GeV2, Wmin = 100 GeV, Wmin = 300 GeV`. Although cut efficiency does tell us that the `pthatmin` cuts into the lowest bin, the lack of statistics at the proposed `3 GeV` would not be idea either, but it is worth trying out.

- `ZEUS_2012_CE_MPIonoff1`: with 10 million events, at `pthatmin = 6 GeV, q2max = 1 GeV2, Wmin = 100 GeV, Wmin = 300 GeV`. Although cut efficiency does tell us that the `pthatmin` cuts into the lowest bin, the lack of statistics at the proposed `3 GeV` would not be idea either, but it is worth trying out. 

- `ZEUS_2001_MPI_onoff2`: with 10 million events, at `pthatmin = 3 GeV, q2max = 1 GeV2, Wmin = 100 GeV, Wmin = 300 GeV`. Significantly less statistics seen however.

- `ZEUS_2012_MPI_onoff2`: with 10 million events, at `pthatmin = 3 GeV, q2max = 1 GeV2, Wmin = 100 GeV, Wmin = 300 GeV`. Significantly less statistics seen however. 

### First parameter search (`pT0ref`)

The parameter's description: 
Want to initially try `MultipartonInteractions:pT0ref` as that's one of the key parameters in a Pythia tune (found in ref 1 to best be 3.00 for EP as opposed to the default 2.28 (for (anti)proton) and 3.30 for LEP). Want to initially try a broad range then a smaller range to observe how significant the deviation is and then tune at a closer value. 

- Initial `pT0ref` run: `[0.5, 1.0, 2.0, 3.0, 4.0, 5.0, 10.0]`

Using the same cut efficiency settings as above (`pthatmin=3`):

- `ZEUS_2001_MPI_pt0ref1`: with 10 million events, at `pthatmin = 3 GeV, q2max = 1 GeV2, Wmin = 100 GeV, Wmin = 300 GeV`. Significantly less statistics seen however.












## References:

1. I. Helenius, Photon-photon and photon-hadron processes in PYTHIA 8 (2017) and https://indico.cern.ch/event/604619/contributions/2438048/attachments/1464137/2262687/IH_photon2017.pdf




