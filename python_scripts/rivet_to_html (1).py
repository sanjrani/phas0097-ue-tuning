import subprocess
import os

all_files = os.listdir()
yoda_files = []

for file_name in all_files:
    
    file_split = file_name.split(".")
    
    if file_split[1] == "yoda":
        
        yoda_files.append("rivet-mkhtml " + file_name + " -o " + file_split[0] + "_plots" + "\n")
    
with open("rivet_to_html.sh", "w+") as rivet_to_html:
    rivet_to_html.writelines(yoda_files)