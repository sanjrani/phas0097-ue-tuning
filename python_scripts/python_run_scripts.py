## Import necessary modules

import numpy as np
import pandas as pd
import os
import subprocess

## RUN CARD initializer

def run_card_initializer(analysis_template_name, phase_cut, parameters, run_card_name):
    """
    Takes input template name for the analysis run card and adds its specific phase space/paramater set
    to a new run card 
    Input: 
    - analysis_template_name, the name of the general input template used for each analysis
    - options_set, list of two lists options (string elements) wanted for
    - run_card_name, name of the run card that's going to be used
    
    """
    
    #### DONE FOR A SPECIFIC SET OF PARAMETERS ALREADY CHOSEN 
    #### NEED A FUNCTION BEFOREHAND TO VARY THESE AND THEN CALL THIS FUNCTION
    
    # open run card name for the analysis in read (+writing) mode
    with open(analysis_template_name, 'r+') as analysis_template:
        
        analysis_template_content = analysis_template.readlines() # list containing each line as string
        
        # save the index for where phase cut and parameters will be written
        for line_num in range(len(analysis_template_content)):
            
            if analysis_template_content[line_num] == "! Phase Cut\n":
                
                phase_cut_line = line_num
            
            elif analysis_template_content[line_num] == "! Parameters\n":
                
                parameter_line = line_num
                
        # add the phase cuts from phase_cut list to lines beneath phase cut heading
        for i in range(len(phase_cut)):
            
            analysis_template_content[phase_cut_line+(i+1)] = phase_cut[i]
            
        # add the parameters from parameters list to lines beneath parameter heading
        for j in range(len(parameters)):
            
            analysis_template_content[parameter_line+(j+1)] = parameters[j]
        
    # open and overwrite modified run card file
    
    with open(run_card_name, "w+") as run_card:
        
        run_card.writelines(analysis_template_content)
        
## BATCH SUBMISSION FILE

def batch_submit_script(run_card_name, output_name, output_directory):
    """
    Run script for each individual batch submission script
    """
    ## BATCH SUBMISSION FILE 
    ## TO THEN BE CALLED BY A FUNCTION THAT SUBMITS THESE JOBS USING QSUB
    
    
    run_card_name_split = run_card_name.split(".")
        
    batch_submit_name = run_card_name_split[0] + ".sh"
    output_output_directory = "output"
    batch_submit_content = []
    
    # Preliminary shenanigans
    # To do: make more general for the walltime and memory
    batch_submit_content.append("#!/bin/sh\n")
    batch_submit_content.append("#PBS -j oe\n")
    batch_submit_content.append("#PBS -l walltime=00:10:00\n")
    batch_submit_content.append("#PBS -l cput=00:01:00\n")
    batch_submit_content.append("#PBS -l mem=200mb\n")
    batch_submit_content.append("#PBS -l nodes=1\n")
    batch_submit_content.append("cd /unix/cedar/sanjrani/MSCI_STUFF/SOFTWARE/pythia8306/examples/\n")
    batch_submit_content.append("./main93 -c " + output_directory + "/" + run_card_name + " -n "+ "100000" + " -o " + output_directory + "/" + output_output_directory + "/" + output_name + "\n")
    
    batch_submit_directory = "/unix/cedar/sanjrani/MSCI_STUFF/BATCH_TEST_AREA/first_zeus/"+output_directory
    with open(batch_submit_directory + "/" + batch_submit_name, 'w+') as batch_submit:
        
        batch_submit.writelines(batch_submit_content)
    
    return batch_submit_name

## QSUB SUBMISSION SCRIPT

def batch_submit_qsub(batch_submits, settings_set_name):
    """
    Writing bash script containing commands to submit the batch submits to the cluster
    Input:
    - batch_submits, name of run job files to be given to cluster
    - settings_set_name, name of the settings job we're running
    Returns:
    - batch_submits_name, name of the bash scrip that sends the jobs off to the cluster
    """
    
    ## FILE TO BE RUN (FIRST NEEDS: chmod +x batch_submits_name)
    ## SUBMITS THE INDIVIDUAL RUNS TO THE CLUSTER 
    
    batch_submits_name = "qsub_submit.sh"
    batch_submit_qsub_content = []
    
    for name in batch_submits:
        batch_submit_qsub_content.append("qsub " + name + "\n")
        batch_submit_qsub_content.append("echo Submitted " + name + "\n")
    
    batch_submit_directory = "/unix/cedar/sanjrani/MSCI_STUFF/BATCH_TEST_AREA/first_zeus/"+settings_set_name
    
    with open(batch_submit_directory + "/" + batch_submits_name, 'w+') as batch_submits_script:
        
        batch_submits_script.writelines(batch_submit_qsub_content)
    
    return batch_submits_name

def run_repeater(settings_set_name, run_card_name, n_repeats):
    """
    repeats a settings set a "n_repeats" amount of times (is an integer)
    creates directory with the settings set name
    creates batch submission files for the number of events
    """
    
    # subprocess.run(["cd", "/unix/cedar/sanjrani/MSCI_STUFF/SOFTWARE/pythia8306/examples"])
    # subprocess.run(["mkdir", settings_set_name])
    
    #subprocess.run(["cd", "/unix/cedar/sanjrani/MSCI_STUFF/BATCH_TEST_AREA/first_zeus"])
    #subprocess.run(["cd", settings_set_name])

    batch_submits = []
    
    for n in range(n_repeats):
        
        run_card_name_split = run_card_name.split(".")
        run_card_name_n = run_card_name_split[0] + "_" + str(n+1) + "." + run_card_name_split[1]
        
        output_name_n = "OUTPUT_" + run_card_name_split[0] + "_" + str(n+1) # name of output files
        
        batch_submit_n = batch_submit_script(run_card_name_n, output_name_n, settings_set_name)
        batch_submits.append(batch_submit_n)
        
    batch_submit_qsub_name = batch_submit_qsub(batch_submits, settings_set_name)

def run_card_repeater(settings_set_name, run_card_name, n_repeats):
    """
    Generates run cards for the run repeater to then use and submit
    Comes after already having a run card for a specific parameter set from run_card_initialiser
    """
    
    output_output_directory = "output"
    
    # output for the run files, output for the output files
    subprocess.run(["mkdir", "/unix/cedar/sanjrani/MSCI_STUFF/SOFTWARE/pythia8306/examples/"+settings_set_name])
    subprocess.run(["mkdir", "/unix/cedar/sanjrani/MSCI_STUFF/SOFTWARE/pythia8306/examples/"+settings_set_name+"/"+output_output_directory])
    
    subprocess.run(["mkdir", "/unix/cedar/sanjrani/MSCI_STUFF/BATCH_TEST_AREA/first_zeus/"+settings_set_name])
    # subprocess.run(["cd", settings_set_name])
    
    with open(run_card_name, 'r+') as run_card:
        
            run_card_content = run_card.readlines()
    
    for n in range(n_repeats):
        #subprocess.run(["cd", "/unix/cedar/sanjrani/MSCI_STUFF/BATCH_TEST_AREA/first_zeus"])
        run_card_content_n = run_card_content
        run_card_content_n.append("\n")
        # ensures random number seed for each iteration
        run_card_content_n.append("Random:setSeed=on\n")
        run_card_content_n.append("Random:seed=" + str(n+1))
        
        run_card_name_split = run_card_name.split(".")
        run_card_name_n = run_card_name_split[0] + "_" + str(n+1) + "." + run_card_name_split[1]
        
        
        # run card directory in pythia examples file (to be run)
        run_card_directory = "/unix/cedar/sanjrani/MSCI_STUFF/SOFTWARE/pythia8306/examples/"+settings_set_name
        
        #subprocess.run(["cd", run_card_directory])
        
        with open(run_card_directory + "/" + run_card_name_n, 'w+') as run_card_n:
            
            run_card_n.writelines(run_card_content_n)
        
    run_repeater(settings_set_name, run_card_name, n_repeats)


run_card_initializer("ZEUS_2001_template.cmnd", ["Photon:Q2max=1\n", "PhaseSpace:pThatMin=10\n"],["\n"], "ZEUS_2001_inaugral_2_repeats.cmnd")
                         
run_card_repeater("ZEUS_2001_inaugral_2_repeats", "ZEUS_2001_inaugral_2_repeats.cmnd", 10)
