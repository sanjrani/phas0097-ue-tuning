import os
import subprocess
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

# except, this currently doesn't plot anything...

## MASTER_PLOTTER to call at end

def master_plotter(analysis_set_name, parameter_or_phase_space):
    ### parameter_or_phase_space = "PARAMETER" or "PHASE_SPACE"
    
    
    ## some initial paths:
    output_file_name = "output"
    current_path = "/users/sanjrani/cedar/MSCI_STUFF/BATCH_AREA_TEST/"
    analysis_set_path = current_path + analysis_set_name
    output_set_path = analysis_set_path + "/" + output_file_name
    
    # getting directory paths for all settings
    # assumes only contents are directories with the settings names
    settings_set_names = os.listdir(output_set_path)
    settings_set_paths = []
    for settings_set in settings_set_names:
        settings_set_path = output_set_path + "/" + settings_set
        settings_set_paths.append(settings_set_path)
    
    # indexing of its paths is same as that of the settings set paths
    all_repeat_directories = [] # directory containing all the dat files
    
    # making the htmls
    for path in settings_set_paths:
        # turn all the yodas into directories with their output
        # returns list of directory names for each repeat
        rivet_output_directories_paths = rivet_to_html_sub(path)
        all_repeat_directories.append(rivet_output_directories_paths)
        
    
    
    # now we've created all the .dat files
    # we also have all the paths to the .dat files directories for each setting
    # e.g., 3 repeats, 2 settings would make a list of 2 elements which are lists of three strings
    
    # now for a given .dat we need to:
    # create a directory in analysis_set/plots/
    # calculate averages for a given setting from its repeats
    # save the averages table somewhere
    # save a plot in the dat file
    # produce a combined plot for the dat file and calculate the chi squares (dep on parameter or phase space)
    # repeat for all dats
    
    # get list of what .dat files there are, these should all be the same for all, so we'll use the first one:
    dat_file_all = os.listdir(all_repeat_directories[0][0])
    dat_file_dat = []
    for file in dat_file_all:
        file_split = file.split(".")
        if file_split[1] == "dat":
            dat_file_dat.append(file)
    
    # create plots directory
    plots_directory_name = "plots"
    plots_directory = analysis_set_path + "/" + plots_directory_name
    subprocess.run(["mkdir", plots_directory])
    
    # now do everything for each .dat file
    for dat_file in dat_file_dat:
        dat_file_split = dat_file.split(".")
        dat_file_name = dat_file_split[0]
        dat_file_directory = plots_directory + "/" + dat_file_name # analysis/plots/dat_y
        
        subprocess.run(["mkdir", dat_file_directory])
        dat_file_everything(dat_file, dat_file_name, dat_file_directory, settings_set_names, all_repeat_directories, parameter_or_phase_space)
        
#########################################################


def dat_file_everything(dat_file, dat_file_name, dat_file_directory, settings_set_names, all_repeat_directories, parameter_or_phase_space):
    """
    For a given dat file, do all this shenanigans:
    # calculate averages for a given setting from its repeats
    # save the averages table somewhere
    # save a plot in the dat file
    # produce a combined plot for the dat file and calculate the chi squares (dep on parameter or phase space)
    """
    
    data_and_mc_log_name = "data_and_mc.log"
    data_and_mc_log = ["DATA AND MC LOG\n", dat_file_name + "\n", "\n"]
    
    
    # for the first run, we'll append the data_list_df values and uncertainties --> becomes True after
    first_setting = True
    
    # setting_x is an index matching settings_set_names and settings_set_paths and all_repeat_directories
    for setting_x in range(len(settings_set_names)):
        
        # df columns: ["bin min", "bin max", "value", "error -", "error +"]
        # averages with uncertainties
        # would return chi squared as well but don't know how to calculate ...
        data_list_df, mc_average_list, mc_average_uncertainties_list = averager(dat_file, all_repeat_directories[setting_x])
        
        if first_setting == True:
            
            data_list_df_bin_min = str(data_list_df["bin min"].tolist())
            data_list_df_bin_max = str(data_list_df["bin max"].tolist())
            data_list_df_value = str(data_list_df["value"].tolist())
            data_list_df_uncertainty = str(data_list_df["error -"].tolist()) # assumes error + is same
            
            data_and_mc_log.append("BIN MIN: " + data_list_df_bin_min + "\n")
            data_and_mc_log.append("BIN MAX: " + data_list_df_bin_max + "\n")
            data_and_mc_log.append("\n")
            data_and_mc_log.append("DATA VALUE: " + data_list_df_value + "\n")
            data_and_mc_log.append("DATA UNCERTAINTY: " + data_list_df_uncertainty + "\n")
            
            # don't need to do the above again
            first_setting = False
        
        data_and_mc_log.append("\n")
        data_and_mc_log.append("SETTING: " + settings_set_name + "\n")
        data_and_mc_log.append("MC VALUE: " + str(mc_average_list) + "\n")
        data_and_mc_log.append("MC UNCERTAINTY: " + str(mc_average_uncertainties_list) + "\n")
        # would also put chi squared goodness of fit
        
        # plot data and save it somewhere (make a whole fat ass file of tables)
        
        # if phase space, calculate the chi squared now:
        
    with open(dat_file_directory + "/" + data_and_mc_log_name, "w+") as data_and_mc_log_file:
        
        data_and_mc_log_file.writelines(data_and_mc_log)
        
#########################################################


def averager(dat_file, repeat_directories):
    """
    Calculates the average of a particular data table
    Input:
    - dat_file_name, name of the specified data table
    - dat_file_directory, directory of the run it was in
    Output:
    - averages, list of the average values for each bin
    To do:
    - incorporate uncertainties
    """
    
    # shape is: nrepeats amount of lists, each list contains bins amounts of elements
    mc_values_list = []
    mc_uncertainties_list = []
    
    # data list always the same, gotta save one, ignore it in the loop
    data_list_0, mc_list_0 = dat_numbers_collector(repeat_directories[0] + "/" + dat_file)
    data_list_df = dat_file_reformatter(data_list_0)
    
    for directory in repeat_directories:
        
        dat_file_path = directory + "/" + dat_file
        data_list, mc_list = dat_numbers_collector(dat_file_path)
        
        mc_list_df = dat_file_reformatter(mc_list)
        
        # fetch the column of values
        # need a better way of doing this (setting up and calculating the average)
        mc_values = mc_list_df["value"]
        mc_uncertainties = mc_list_df["error -"] # error always same +/- (change if this not the case)
        
        mc_values_list.append(mc_values)
        mc_uncertainties_list.append(mc_uncertainties)
        
    # the final lists we want to return
    mc_average_list = []
    mc_average_list_uncertainties = []
    
    # calculate average and the uncertainties
    for bin_index in range(len(mc_values_list[0])):
        
        bin_average = 0
        bin_average_uncertainty = 0
        
        for value_index in range(len(mc_values_list)):
            
            bin_average += mc_values_list[value_index][bin_index] / len(mc_values_list)
            bin_average_uncertainty += ((mc_uncertainties_list[value_index][bin_index])**2)
        
        bin_average_uncertainty_final = np.sqrt(bin_average_uncertainty) / len(mc_values_list)
        
        mc_average_list.append(bin_average)
        mc_average_list_uncertainties(bin_average_uncertainty_final)
    
    # would calculate chi squared here
    mc_average_list
    
    return data_list_df, mc_average_list, mc_average_list_uncertainties
    
#########################################################


def dat_numbers_collector(dat_file_name):
    """
    Takes the .dat file name from a given  as input, and returns as output the tables of data in the form of arrays
    """
    
    table_top = "# xlow\t xhigh\t val\t errminus\t errplus\n"
    
    table_top_index = [] # will have indices at which the data table and mc tables begin (2 values)
    
    table_bottom = "# END HISTO1D\n"
    
    data_list = []
    
    mc_list = []
    
    with open(dat_file_name, 'r') as dat_file:
        
        dat_file_content = dat_file.readlines()
        
        for line_num in range(len(dat_file_content)):
            
            if dat_file_content[line_num] == table_top:
                
                table_top_index.append(line_num)
                
        # read the lines in the table and split them based on the \t delimeter
        # append resultant list into data_list to be returned
        i = 1 # counter
        while dat_file_content[table_top_index[0] + i] != table_bottom:
            
            line_content = dat_file_content[table_top_index[0] + i]
            
            line_content = line_content.replace("\n", "") # removes "new line" at end of each string
            
            split_line_content = line_content.split("\t")
            
            data_list.append(split_line_content)
            
            i += 1
        
        
        # read the lines in the table and split them based on the \t delimeter
        # append resultant list into mc_list to be returned
        i = 1 # counter
        while dat_file_content[table_top_index[1] + i] != table_bottom:
            
            line_content = dat_file_content[table_top_index[1] + i]
            
            line_content = line_content.replace("\n", "") # removes "new line" at end of each string
            
            split_line_content = line_content.split("\t")
            
            mc_list.append(split_line_content)
            
            i += 1
            
    return data_list, mc_list
    
#########################################################

def dat_file_reformatter(table):
    """
    Reformats the dat file tables to pandas dataframes (handles one table at a time)
    Also converts from datatype from strings to floats
    Input:
    - data list
    """
    table_columns = ["bin min", "bin max", "value", "error -", "error +"]
    table_df = pd.DataFrame(columns=table_columns, data=table)
    
    # converting from string to floats
    for column in table_columns:
        table_df[column] = pd.to_numeric(table_df[column], downcast="float")
    
    return table_df
    
#########################################################


def rivet_to_html_sub(path):
    """
    Converts yoda files in a given directory to rivet output
    Input is the path containing all the .log and .yoda files
    (assumes that those are the only ones there)
    Output all contained within different directories for each run
    Returns list of path of directories created
    """
    all_files = os.listdir(path)
    file_output_paths = []
    file_output_paths_dat = [] # where all the dat files are contained
    for file_name in all_files:
        
        file_split = file_name.split(".")
        
        # if we find a yoda file, we'll rivet-mkhtml this
        if file_split[1] == "yoda":
            file_output_name = path + "/" + file_split[0]
            file_output_path = file_output_name + "_plots"
            file_output_paths.append(file_output_path)
            file_path = path + "/" + file_name
            
            subprocess.run(["echo", "making html..."])
            subprocess.run(["rivet-mkhtml", file_name, "-o", file_output_path])
    
    # getting name of directory inside the rivet-mkhtml output
    two_files = os.listdir(file_output_paths[0])
    for file in two_files:
        if os.path.isdir(os.path.join(".", item)):
            dat_directory_name = file
    
    for output_path in file_output_paths:
        dat_path = output_path + "/" + dat_directory_name
        file_output_paths_dat.append(dat_path)
        
    return file_output_paths_dat
    
#########################################################


def rivet_to_html_sh(path):
    """
    Converts yoda files in a given directory to rivet output
    Input is the path containing all the .log and .yoda files
    (assumes that those are the only ones there)
    Output all contained within different directories for each run
    Returns list of path of directories created
    """
    all_files = os.listdir(path)
    yoda_files = []
    
    # necessary as the subprocess situation uses a whole bash shell
    # which, without the rivet environment, won't run rivet-mkhtml
    sourcing_rivet = "source /unix/cedar/software/cos7/Herwig-repo_Rivet-repo/setupEnv.sh\n"
    yoda_files.append(sourcing_rivet)
    
    file_output_paths = []
    
    for file_name in all_files:
        
        file_split = file_name.split(".")
        
        # if we find a yoda file, we'll rivet-mkhtml this
        if file_split[1] == "yoda":
            file_output_name = file_split[0] + "_plots"
            file_output_path = path + "/" file_output_name
            file_output_paths.append(file_output_path)
            
            yoda_files.append("rivet-mkhtml " + file_name + " -o " + file_output_path + "\n")
            
    rivet_to_html_sh_name = "rivet_to_html.sh"
    path_sh = path + "/" + rivet_to_html_sh_name
    with open(path_sh, "w+") as rivet_to_html_sh:
        rivet_to_html_sh.writelines(yoda_files)
    
    subprocess.run(["echo", "Running the rivet_to_html script in " + path + " ..."])
    subprocess.run(["bash", "-c", "source " + path_sh])
    
    return file_output_paths

