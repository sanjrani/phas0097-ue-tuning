## Import necessary modules

import numpy as np
import pandas as pd
import os
import subprocess


# analysis_set_x = ["set_name", parameters_set, phase_space_set, number_of_repeats, number_of_events, "analysis_name"]

def master_run(analysis_set):
    """
    Currently only allows for one element of phase_space_set or parameters_set to be varied
    Basically finds out what things to vary, and calls the settings run for the correct procedure
    """
    # some things to unpack
    analysis_set_name = analysis_set[0]
    analysis_set_parameters_set = analysis_set[1]
    analysis_set_phase_space_set = analysis_set[2]
    analysis_set_nrepeats = analysis_set[3]
    analysis_set_nevents = analysis_set[4]
    analysis_set_analysis_name = analysis_set[5]
    
    # obtain paths for the batch directory [0] and for the analysis_set directory [1]
    # and its input [2] and output directories [3]
    paths = directories_initializer(analysis_set_name, analysis_set_analysis_name)
    
    thing_to_vary = 0 # default if nothing being varied, but is the list of varied settings usually
    
    # wanna check whether the thing to vary is a phase space setting or a parameter setting
    is_parameter = False
    is_phase_space = False
    
    # check what is being varied here:
    for parameter_i in range(len(analysis_set_parameters_set)):
        
        if type(analysis_set_parameters_set[parameter_i]) is list:
            
            # to do: maybe incorporate this with the name of what parameter is being varied?
            # i.e., generalise the code
            print("Parameter being varied: ", analysis_set_parameters_set[parameter_i])
            thing_to_vary = analysis_set_parameters_set[parameter_i]
            is_parameter = True
            index_i = parameter_i
    
    for phase_space_i in range(len(analysis_set_phase_space_set)):
        
        if type(analysis_set_phase_space_set[phase_space_i]) is list:
            
            # to do: maybe incorporate this with the name of what parameter is being varied?
            # i.e., generalise the code
            print("Phase space being varied: ", analysis_set_phase_space_set[phase_space_i])
            thing_to_vary = analysis_set_phase_space_set[phase_space_i]
            is_phase_space = True
            index_i = phase_space_i
    
    settings_set_names = ["\n"] # will write this list of settings_set names and their indexes to a file for logging
    
    # if there is something to vary, run each parameter set seperately
    # if not, just run this analysis_set as the parameter set
    if thing_to_vary != 0:
        
    # parameter_x and phase_space_x are indexes of the thing_to_vary list
    
        if is_parameter == True:
            
            settings_set_parameters_set = analysis_set_parameters_set.copy()
            
            for parameter_x in range(len(thing_to_vary)):
                
                settings_set_parameters_set[index_i] = thing_to_vary[parameter_x]
                settings_set_x = [analysis_set_name, settings_set_parameters_set, analysis_set_phase_space_set, analysis_set_nrepeats, analysis_set_nevents, analysis_set_analysis_name]
                settings_set_x_name = analysis_set_name + "_" + parameter_x
                settings_set_names.append(settings_set_x_name + " - " + settings_set_parameters_set[index_i] + "\n")
                settings_index = parameter_x
                
                settings_run(settings_set_x, settings_set_x_name, paths, settings_index)
        
        elif is_phase_space == True:
            
            settings_set_phase_space_set = analysis_set_phase_space_set.copy()
            
            for phase_space_x in range(len(thing_to_vary)):
                
                settings_set_phase_space_set[index_i] = thing_to_vary[phase_space_x]
                settings_set_x = [analysis_set_name, analysis_set_parameters_set, settings_set_phase_space_set, analysis_set_nrepeats, analysis_set_nevents, analysis_set_analysis_name]
                settings_set_x_name = analysis_set_name + "_setting" + phase_space_x
                settings_set_names.append(settings_set_x_name + " - " + settings_set_phase_space_set[index_i] + "\n")
                settings_index = phase_space_x
                
                settings_run(settings_set_x, settings_set_x_name, paths, settings_index)
        
    else:
        
        settings_set_name = analysis_set_name
        settings_index = 0
        settings_run(settings_set, settings_set_name, paths, 0)
    
    settings_set_names_log_name = paths[1] + "/" + analysis_set_name + ".txt"
    
    with open(settings_set_names_log_name, 'w+') as settings_set_names_log:
        
            settings_set_names_log.writelines(settings_set_names)
            
    ###############################################################

def directories_intializer(analysis_set, analysis_template):
    """
    unpack the info from the analysis_set and create a bunch of directories and paths related to it
    """
    # some things to unpack
    analysis_set_name = analysis_set[0]
    
    path_current = "/unix/cedar/sanjrani/MSCI_STUFF/BATCH_TEST_AREA/"
    path_analysis_set = path_current + analysis_set_name
    path_input = path_analysis_set + "/" + "input"
    path_output = path_analysis_set + "/" + "output"
    
    subprocess.run(["mkdir", path_analysis_set]) # analysis_set_x directory
    subprocess.run(["mkdir", path_input]) # analysis_set_x/input directory
    subprocess.run(["mkdir", path_output]) # analysis_set_x/output directory
    subprocess.run(["cp", path_output]) # analysis_set_x/output directory
    
    paths = [path_batch, path_analysis_set, path_input, path_output]
    
    return paths
    
    ###############################################################


## RUN FILE FOR EACH SETTING

def settings_run(settings_set, settings_set_name, paths, settings_index):
    """
    Basically did what the run_card_repeater did for a particular settings run
    Input:
    - paths: [0] for analysis set, [1] for analysis/input directory, [2] for analysis/output directory
    """
    # unpacked meaning
    #settings_set_name = analysis_set[0]
    #settings_set_parameters_set = analysis_set[1]
    #settings_set_phase_space_set = analysis_set[2]
    #settings_set_nrepeats = analysis_set[3]
    #settings_set_nevents = analysis_set[4]
    #settings_set_analysis_name = analysis_set[5]
    
    # create directory for this setting inputs:
    settings_set_path_input = paths[2] + "/" + settings_set_name
    subprocess.run(["mkdir", settings_set_path])
    
    # create directory for this setting outputs:
    settings_set_path_output = paths[3] + "/" + settings_set_name
    subprocess.run(["mkdir", settings_set_path])
    
    
    paths_settings = paths.copy()
    paths_settings.append(settings_set_path_input) # paths + settings_path_input
    paths_settings.append(settings_set_path_output) # paths + settings_path_output
    
    # paths settings: [0] batch, [1] batch/analysis_set
    # paths settings: [2] analysis_set/input, [3] analysis_set/output
    # paths settings: [4] input/settings_set_x, [5] output/settings_set_x
    
    # run card maker for a setting set
    analysis_template = settings_set[5]
    parameters = settings_set[1]
    phase_space = settings_set[2]
    run_card_name = settings_set_name + "_RUN.cmnd"
    
    run_card_initializer(analysis_template, parameters, phase_space, run_card_name, paths_settings)

    run_card_path = paths_settings[4] + "/" + run_card_name
    
    # reads the settings run card to be used for the run repeat
    with open(run_card_path, 'r+') as run_card:
        
            run_card_content = run_card.readlines()
    
    nrepeats = settings_set[3]
    nevents = settings_set[4]
    
    run_card_directory = settings_set_path + "/run_cards"
    subprocess.run(["mkdir", run_card_directory])
    
    # create run card for each repeat
    # (making seed increment by 1 each time right now)
    run_repeater_seeder(nrepeats, run_card_content, run_card_directory)
    
    # get qsub submission file name and also all the shenanigans with:
    # creating submission job file for each repeat
    # creating submission job file that submits all the repeat jobs at once to the farm
    
    batch_submit_qsub_name = run_repeater(nrepeats, settings_set_name, run_card_name, run_card_directory, paths_settings[5], settings_set[4])
    
    # making submission job executable and running
    subprocess.run(["chmod", batch_submit_directory+"/"+batch_submit_qsub_name])
    subprocess.run(["echo", "Submitting " + settings_set[0] " ..."])
    subprocess.run(["source", batch_submit_directory+"/"+batch_submit_qsub_name])
    subprocess.run(["echo", "done :)"])
    
    #############################################################
    

def run_repeater_seeder(nrepeats, run_card_content, run_card_directory):
    
    for n in range(nrepeats):
        
        run_card_content_n = run_card_content.copy()
        run_card_content_n.append("\n")
        # ensures random number seed for each iteration
        run_card_content_n.append("Random:setSeed=on\n")
        run_card_content_n.append("Random:seed=" + str(n+1))
        
        run_card_name_split = run_card_name.split(".")
        run_card_name_n = run_card_name_split[0] + "_repeat" + str(n+1) + "." + run_card_name_split[1]
        
        run_card_name_n_path = run_card_directory + "/" + run_card_name_n
        
        with open(run_card_name_n_path, 'w+') as run_card_n:
        
            run_card_n.writelines(run_card_content_n)

    #############################################################

def run_repeater(nrepeats, settings_set_name, run_card_name, run_card_directory, output_directory_settings_path, nevents):
    """
    repeats a settings set a "n_repeats" amount of times (is an integer)
    creates directory with the settings set name
    creates batch submission files for the number of events
    """

    batch_submits = []
    
    batch_submit_directory = paths_settings[4] + "/batch_submit"
    subprocess.run(["mkdir", batch_submit_directory])
    

    for n in range(nrepeats):
        
        run_card_name_split = run_card_name.split(".")
        run_card_name_n = run_card_name_split[0] + "_repeat" + str(n+1) + "." + run_card_name_split[1]
        
        output_name_n = "OUTPUT_" + run_card_name_split[0] + "_repeat" + str(n+1) # name of output files
        
        batch_submit_n = batch_submit_script(run_card_name_n, output_name_n, batch_submit_directory, run_card_directory, output_directory_settings_path, nevents)
        batch_submits.append(batch_submit_n)
        
    batch_submit_qsub_name = batch_submit_qsub(batch_submit_directory, batch_submits)
    
    return batch_submit_qsub_name
    
    #############################################################
    

def batch_submit_script(run_card_name, output_name, batch_submit_directory, run_card_directory, output_directory_settings_path):
    """
    Run script for each individual batch submission script
    """
    ## BATCH SUBMISSION FILE
    ## TO THEN BE CALLED BY A FUNCTION THAT SUBMITS THESE JOBS USING QSUB
    
    
    run_card_name_split = run_card_name.split(".")
        
    batch_submit_name = run_card_name_split[0] + ".sh"
 
    batch_submit_content = []
    
    # Preliminary shenanigans
    # To do: make more general for the walltime and memory
    # numbers right now work really just for 100k events
    batch_submit_content.append("#!/bin/sh\n")
    batch_submit_content.append("#PBS -j oe\n")
    batch_submit_content.append("#PBS -l walltime=00:10:00\n")
    batch_submit_content.append("#PBS -l cput=00:05:00\n")
    batch_submit_content.append("#PBS -l mem=200mb\n")
    batch_submit_content.append("#PBS -l nodes=1\n")
    batch_submit_content.append("cd /unix/cedar/sanjrani/MSCI_STUFF/SOFTWARE/pythia8306/examples/\n")
    batch_submit_content.append("./main93 -c " + run_card_directory + "/" + run_card_name + " -n "+ str(nevents) + " -o " + output_directory_settings_path + "/" + output_name + "\n")
    
    with open(batch_submit_directory + "/" + batch_submit_name, 'w+') as batch_submit:
        
        batch_submit.writelines(batch_submit_content)
    
    return batch_submit_name

    #############################################################

def batch_submit_qsub(batch_submit_directory, batch_submits):
    """
    Writing bash script containing commands to submit the batch submits to the cluster
    Input:
    - batch_submits, name of run job files to be given to cluster
    - settings_set_name, name of the settings job we're running
    Returns:
    - batch_submits_name, name of the bash scrip that sends the jobs off to the cluster
    """
    
    ## FILE TO BE RUN (FIRST NEEDS: chmod +x batch_submits_name)
    ## SUBMITS THE INDIVIDUAL RUNS TO THE CLUSTER
    
    batch_submits_name = "qsub_submit.sh"
    batch_submits_qsub_content = []
    batch_submits_qsub_content.append("cd " + batch_submit_directory + "\n")
    
    for name in batch_submits:
        batch_submits_qsub_content.append("qsub " + name + "\n")
        batch_submits_qsub_content.append("echo Submitted " + name + "\n")
    
    with open(batch_submit_directory + "/" + batch_submits_name, 'w+') as batch_submits_script:
        
        batch_submits_script.writelines(batch_submits_qsub_content)
    
    return batch_submits_name

    #############################################################
    
def run_card_initializer(analysis_template_name, parameters, phase_space, run_card_name, paths_settings):
    """
    Takes input template name for the analysis run card and adds its specific phase space/paramater set
    to a new run card
    """
    
    #### DONE FOR A SPECIFIC SET OF PARAMETERS ALREADY CHOSEN
    #### NEED A FUNCTION BEFOREHAND TO VARY THESE AND THEN CALL THIS FUNCTION
    
    analysis_template_path = paths_settings[0] + analysis_template_name + ".cmnd"
    
    # open run card name for the analysis in read (+writing) mode
    with open(analysis_template_path, 'r+') as analysis_template:
        
        analysis_template_content = analysis_template.readlines() # list containing each line as string
        
        # save the index for where phase cut and parameters will be written
        for line_num in range(len(analysis_template_content)):
            
            if analysis_template_content[line_num] == "! Phase Cut\n":
                
                phase_space_line = line_num
            
            elif analysis_template_content[line_num] == "! Parameters\n":
                
                parameter_line = line_num
                
        # add the phase cuts from phase_cut list to lines beneath phase cut heading
        for i in range(len(phase_space)):
            
            analysis_template_content[phase_space_line+(i+1)] = phase_space[i]
            
        # add the parameters from parameters list to lines beneath parameter heading
        for j in range(len(parameters)):
            
            analysis_template_content[parameter_line+(j+1)] = parameters[j]
        
    # open and overwrite modified run card file
    run_card_path = paths_settings[4] + "/" + run_card_name
    
    with open(run_card_path, "w+") as run_card:
        
        run_card.writelines(analysis_template_content)
        
    #############################################################
