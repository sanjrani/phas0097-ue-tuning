import os
import subprocess
import numpy as np
import pandas as pd
import matplotlib.pyplot as plt

## RETURNS OUTPUT TABLES OF THE .DAT FILE

def dat_numbers_collector(dat_file_name):
    """
    Takes the .dat file name from a given  as input, and returns as output the tables of data in the form of arrays
    """
    
    table_top = "# xlow\t xhigh\t val\t errminus\t errplus\n"
    
    table_top_index = [] # will have indices at which the data table and mc tables begin (2 values)
    
    table_bottom = "# END HISTO1D\n"
    
    data_list = []
    
    mc_list = []
    
    with open(dat_file_name, 'r') as dat_file:
        
        dat_file_content = dat_file.readlines()
        
        for line_num in range(len(dat_file_content)):
            
            if dat_file_content[line_num] == table_top:
                
                table_top_index.append(line_num)
                
        # read the lines in the table and split them based on the \t delimeter
        # append resultant list into data_list to be returned
        i = 1 # counter
        while dat_file_content[table_top_index[0] + i] != table_bottom:
            
            line_content = dat_file_content[table_top_index[0] + i]
            
            line_content = line_content.replace("\n", "") # removes "new line" at end of each string
            
            split_line_content = line_content.split("\t")
            
            data_list.append(split_line_content)
            
            i += 1
        
        
        # read the lines in the table and split them based on the \t delimeter
        # append resultant list into mc_list to be returned
        i = 1 # counter
        while dat_file_content[table_top_index[1] + i] != table_bottom:
            
            line_content = dat_file_content[table_top_index[1] + i]
            
            line_content = line_content.replace("\n", "") # removes "new line" at end of each string
            
            split_line_content = line_content.split("\t")
            
            mc_list.append(split_line_content)
            
            i += 1
            
    return data_list, mc_list

## RETURNS 

def dat_file_reformatter(table):
    """
    Reformats the dat file tables to pandas dataframes (handles one table at a time)
    Also converts from datatype from strings to floats
    Input:
    - data list
    """
    table_columns = ["bin min", "bin max", "value", "error -", "error +"]
    table_df = pd.DataFrame(columns=table_columns, data=table)
    
    # converting from string to floats
    for column in table_columns:
        table_df[column] = pd.to_numeric(table_df[column], downcast="float")
    
    return table_df

def averager(dat_file_name):
    """
    Calculates the average of a particular data table
    Input:
    - dat_file_name, name of the specified data table
    - dat_file_directory, directory of the run it was in
    Output:
    - averages, list of the average values for each bin
    To do:
    - incorporate uncertainties
    """
    directory_list = []
    mc_values_list = []
    
    for item in os.listdir():
        if os.path.isdir(os.path.join(".", item)):
            directory_list.append(item)
    
    for directory in directory_list:
        
        dat_file = directory + "/OPAL_2008_I754316/" + dat_file_name
        data_list, mc_list = dat_numbers_collector(dat_file)
        
        mc_list_df = dat_file_reformatter(mc_list)
        data_list_df = dat_file_reformatter(data_list)
        
        # fetch the column of values
        # need a better way of doing this (setting up and calculating the average)
        mc_values = mc_list_df["value"]
        mc_values_list.append(mc_values)
        
    mc_average_list = [] # the final list we want to return
    
    for bin_index in range(len(mc_values_list[0])):
        
        bin_average = 0
        
        for value_index in range(len(mc_values_list)):
            
            bin_average += mc_values_list[value_index][bin_index]
        
        bin_average = bin_average / len(mc_values_list)
        
        mc_average_list.append(bin_average)
    
    return data_list_df, mc_average_list

def plotter(dat_file_name):
    
    data_list_df, mc_average_list = averager(dat_file_name)
    
    x_array = (np.array(data_list_df['bin max']) + np.array(data_list_df['bin min']))/2
    y_array_data = np.array(data_list_df['value'])
    y_error_data = data_list_df['error -']
    
    y_array_mc_average = mc_average_list
    bin_widths = (np.array(data_list_df['bin max']) - np.array(data_list_df['bin min']))/2
    
    plt.errorbar(x_array, y_array_data, yerr=y_error_data, xerr=bin_widths, fmt='k.')
    plt.errorbar(x_array, y_array_mc_average, yerr=None, xerr=bin_widths, fmt='r.')
    
    dat_file_name_split = dat_file_name.split(".")
    
    plt.title(dat_file_name_split[0] + " where MC = red, data = black")
    plt.yscale("log")
 
    plt.savefig(dat_file_name_split[0] + ".png")
    
plotter("d02-x01-y01.dat")

