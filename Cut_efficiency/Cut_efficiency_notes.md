# PHAS0097 UE Tuning - Cut Efficiency Notes

This is on a cut efficiency analysis, which is needed to see what cuts are still inclusive enough to contain all relevant events in the analysis

### Date: 23 Jan 2022

***

## 1. Introduction 

**Things to note:** 

- 6 available analyses, three LEP and three HERA.

- One of the HERA ones do not work right now, hence, cannot be used.

- The others have an issue with the number of events that can be generated (tend to fail every time when generating 1,000,000 events)

- Hence, aim to generate 100,000 events 10 times for each HERA routine as it’s less likely to be within 100,000 events but guaranteed to occur within 1,000,000. Still happens to fail a lot and aim to get around at least 5/10 of them to successfully produce events. 

- Need to automate code a little to allow for this to be easier, as the multi-step process is a little cumbersome and disheartening

**Strategy:**

- Make code more automated and less run-dependent, ideally want to just be able to select the analysis, the phase cut space, and the number of repeats and number of events

- Find out if practically, the uncertainties of repeating 10 times are less than the uncertainties associated with just 1 million events (need to do error propagation) (only able to do this for the LEP routines currently) but need to also find the underlying explanation for why it would be like this … (how are uncertainties in Rivet even calculated?)

- Propagation of uncertainties for bins: in the example of 10 repeat runs, it would be = ( square root ( sum of uncertainties squared ) ) / 10

- Actual cut efficiency strategy should be this: since Pythia default phase cuts aren’t actually the most inclusive, start with the most inclusive and then vary each parameter separately whilst comparing to the most inclusive run. For phase cuts, they can be done separately as they should be independent and have no correlation on the other ones, it’s just a matter of finding the best ones.

- Biggest culprit is `pTHatmin` (start with a wide parameter space) and focus in on a section where it starts cutting and choose just before that then look at `Photon:W2min` and `Photon:Q2min` (not even sure if these are phase space cuts since their defaults are non-inclusive so I’ve asked Ilkka about this)?

- Asked Ilkka and he said that the `Photon:Q2max` and `Photon:Wmin` parameters are phase-space cuts that should match the experimental analysis and that the only photoproduction-specific tunable parameters are related to pT0.



***

## 2. Doing

**So:**

- for now, cannot do phase cut efficiency on the ZEUS files because the low `pTHatmin` cut makes the DIS kinematics issue even likelier and there’s no chance in 100,000 events that this wouldn’t show up

- will do the `ptHatmin` phase cut on the LEP files so far as it should be the most effective anyway


## 3. `pTHatmin` Cuts

Doing this on an analysis by analysis basis, as each analysis uses different cuts, so it is important to approach them with focus on each one.

### `OPAL_2003_I611415` 
** Dijet production in Photon-Photon collisions at $ E_CMS = 198 GeV $ **

No phase cuts really shown apart from the mean transverse energy of the dijet system as >= 5 GeV, and the invariant di-jet mass as > 15 GeV. 

These correlate with `PhaseSpace:pTHatmin` and `PhaseSpace:mHatmin` which are the minimum pT and the minimum invariant mass, respectively. But they are not equal.

Hence the plan for this is to vary the `pTHatmin` from 0 - 10 GeV in increments of 1 and observe whether there are any discrepencies. THis should 


