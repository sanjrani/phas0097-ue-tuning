# PHAS0097 UE Tuning - Cut Efficiency Notes

This is on a cut efficiency analysis, which is needed to see what cuts are still inclusive enough to contain all relevant events in the analysis

### Date: 31 Jan 2022

***

## 1. Introduction 

**Things to note:** 

- 6 available analyses, three LEP and three HERA.

- One of the HERA ones do not work right now, hence, cannot be used.

- The others have an issue with the number of events that can be generated (tend to fail every time when generating 1,000,000 events)

- Hence, aim to generate 100,000 events 10 times for each HERA routine as it’s less likely to be within 100,000 events but guaranteed to occur within 1,000,000. Still happens to fail a lot and aim to get around at least 5/10 of them to successfully produce events. 

- Need to automate code a little to allow for this to be easier, as the multi-step process is a little cumbersome and disheartening

- Based on meeting on 27 Jan 2022 with Professor Wing, the strategy should consist of a simpler analysis. No need for a chi-squared test, more-so just get a bunch of ptHatmins for the hard process routines as that's what they test and get the safest value from the paper/analysis

- With Q2max and the Wmin --> these should be based on the value in the analyses


## 2. Strategy

**Generally**

Will try to do the LEP routines first, then the ZEUS routines, then the most recent ZEUS routine (as that hasn't got hard events, with the ptmin being very low, so as to include soft events)


### L3_2004_I661114

Starting with a simple one (only has one plot in it)

Based on the paper, this has: 

- pT range: `3 < pT < 50 GeV`
- photon-photon invariant mass W: `W ≥ 5 GeV`
- photon virtuality Q2: `Q2 < 8 GeV2`

Will therefore use the following settings:

- pT: pThatmin range: `0.0, 0.5, 1.0, 1.5, 2.0, 2.5, 3.0, 3.5`
- photon-photon invariant mass W: `Wmin: 5 GeV` (minimum possible)
- photon virtuality Q2: `Q2max = 2 GeV2` (maximum possible)

Therefore, this will be the first ever run, this will be the `analysis_set` call and I will generate 10 million events per `pThatmin` setting to make uncertainties tiny:

Note: `CE` refers to cut efficiency

- `analysis_set_name = "L3_2004_CE_pthatmin1"`
- `analysis_parameters_set = ["\n"]`
- `analysis_phase_space_set = ["Photon:Q2max=2\n", "Photon:Wmin=5\n", ["PhaseSpace:pThatMin=0.0\n", "PhaseSpace:pThatMin=0.5\n", "PhaseSpace:pThatMin=1.0\n", "PhaseSpace:pThatMin=1.5\n", "PhaseSpace:pThatMin=2.0\n", "PhaseSpace:pThatMin=2.5\n", "PhaseSpace:pThatMin=3.0\n", "PhaseSpace:pThatMin=3.5\n" ]]"`
- `analysis_nrepeats = 10`
- `analysis_nevents = 1000000`
- `analysis_analysis = "L3_2004_I661114"`

Update: accidentally deleted one of the yoda files... no way to recreate it, could I suppose get rid of that repeat? Makes it 9 repeats instead ... should still have enough events? And it's not that deep anyway considering that this is only a run to check pThatmin is good...


### OPAL_2008_I754316

Based on the paper, some key things to note:
 
- luminosity-weighted average `eCM = 198.5 GeV`
- pT: `pT > 5 GeV` (based on the routine plots: the range is `5-40 GeV`)
- No other phase-space cuts mentioned (use most inclusive cross-section?)

Hence, will use the following settings:

- pT: pThatmin range: `1.0, 2.0, 3.0, 4.0, 5.0, 6.0`
- photon-photon invariant mass W: `Wmin: 5 GeV` (minimum possible)
- photon virtuality Q2: `Q2max = 2 GeV2` (maximum possible)

This will be the second ever run (but first ever `OPAL_2008`) run, I will also generate 10 million events per `ptHatmin` setting to make uncertainties tiny

- `analysis_set_name = "OPAL_2008_CE_pthatmin1"`
- `analysis_parameters_set = ["\n"]`
- `analysis_phase_space_set = ["Photon:Q2max=2\n", "Photon:Wmin=5\n", ["PhaseSpace:pThatMin=1.0\n", "PhaseSpace:pThatMin=2.0\n", "PhaseSpace:pThatMin=3.0\n", "PhaseSpace:pThatMin=4.0\n", "PhaseSpace:pThatMin=5.0\n", "PhaseSpace:pThatMin=6.0\n" ]]"`
- `analysis_nrepeats = 10`
- `analysis_nevents = 1000000`
- `analysis_analysis = "OPAL_2008_I754316"`

### OPAL_2003_I611415

Based on the paper:

- Dijet events selected by requiring a transverse energy `Et_jet > 3 GeV`
- Quasi-real photons defined by `Q2 < 4.5 Gev2`
- Dijet invariant mass `Mjj > 15 GeV` used 

In the analysis plots, the

Hence, will use the following settings:

- pT: pThatmin range: `1.0, 2.0, 3.0, 4.0, 5.0`
- photon-photon invariant mass W: `Wmin: 5 GeV` (minimum possible)
- photon virtuality Q2: `Q2max = 2 GeV2` (maximum possible)

This will be the third run (first ever `OPAL_2003`), I did this one last because it has tonnes of plots and I know that it's gonna take yonks to do my whole program on it. Nevertheless, here we go:

- `analysis_set_name = "OPAL_2003_CE_pthatmin1"`
- `analysis_parameters_set = ["\n"]`
- `analysis_phase_space_set = ["Photon:Q2max=2\n", "Photon:Wmin=5\n", ["PhaseSpace:pThatMin=0.0\n", "PhaseSpace:pThatMin=1.0\n", "PhaseSpace:pThatMin=2.0\n", "PhaseSpace:pThatMin=3.0\n", "PhaseSpace:pThatMin=4.0\n", "PhaseSpace:pThatMin=5.0\n" ]]"`
- `analysis_nrepeats = 10`
- `analysis_nevents = 1000000`
- `analysis_analysis = "OPAL_2003_I611415"`

### ZEUS_2001_S4815815

First ZEUS paper, got to be careful here because if too inclusive, then we'll produce events that would be much more likely to contain pLepout events (therefore may enter realm of those events occuring a non-negligible amount of times). Unlikely for low, low pT to be included in analysed events as there's a cut for the minimum transverse energy of the jets. 

From the paper:

- Photon virtuality `Q2 < 1 GEV2` defines photoproduction regime
- Photon-proton centre of mass energy `134 < W_gamma_p < 277 GeV`
- Dijet mass `Mjj > 42 GeV`
- Minimum transverse jet energy `Et_jet ≥ 12.5 GeV`
- Minimum transverse jet energy 1 `Et_jet_1 ≥ 14.0 GeV`
- Minimum transverse jet energy 2 `Et_jet_2 ≥ 11.0 GeV`

So we see that minimum transverse jet energy is around `10 GeV`, so there is a considerable cut. `pThatmin` doesn't translate exactly, but correlates so there is room for cutting to make this more efficient.

Ergo, will use the following settings, and will be varying pThatmin. I will be setting `Photon:Wmin and Photon:Wmax` as MPI framework with photons mentions the initialization of several `W` values between the two boundaries, but I am unsure about whether the same invariant masses correspond to calculation used by Rivet. The fact that this cut has not been explicitly used in the routine The parameter values are then interpolated with the sampled `W`:

- pT: `pThatmin: 1.0, 2.0, 3.0, 4.0, 5.0, 6.0, 7.0, 8.0, 9.0, 10.0`
- photon-proton minimum invariant mass W: `Wmin: 100 GeV`
- photon-proton minimum invariant mass W: `Wmax: 300 GeV`
- photon virtuality Q2: `Q2max = 1 GeV2` (maximum possible)


This will be the fourth run (first ever `ZEUS_2001`):

- `analysis_set_name = "ZEUS_2001_CE_pthatmin1"`
- `analysis_parameters_set = ["\n"]`
- `analysis_phase_space_set = ["Photon:Q2max=1\n", "Photon:Wmin=100\n", "Photon:Wmax=300\n", ["PhaseSpace:pThatMin=1.0\n", "PhaseSpace:pThatMin=2.0\n", "PhaseSpace:pThatMin=3.0\n", "PhaseSpace:pThatMin=4.0\n", "PhaseSpace:pThatMin=5.0\n", "PhaseSpace:pThatMin=6.0\n", "PhaseSpace:pThatMin=7.0\n", "PhaseSpace:pThatMin=8.0\n", "PhaseSpace:pThatMin=9.0\n", "PhaseSpace:pThatMin=10.0\n" ]]"`
- `analysis_nrepeats = 10`
- `analysis_nevents = 1000000`
- `analysis_analysis = "ZEUS_2001_S4815815"`

### ZEUS_2012_I1116258

Pretty similar story to the above one. Pythia MPI mentioned though (`psec_T_min` value varied?) to test UE

Paper states:

- Photon virtuality `Q2 < 1 GeV2`
- Photon-proton centre of mass energy `142 < W_gamma_p < 293 GeV`
- Minimum transverse jet energy `Et_jet ≥ 17 GeV`

Note that `W_gamma_p = sqrt(sy)` where `y` is inelasticity. 

Hence, I'll use the same settings as for the `ZEUS_2001_CE_pthatmin1` run:

- `analysis_set_name = "ZEUS_2012_CE_pthatmin1"`
- `analysis_parameters_set = ["\n"]`
- `analysis_phase_space_set = ["Photon:Q2max=1\n", "Photon:Wmin=100\n", "Photon:Wmax=300\n", ["PhaseSpace:pThatMin=1.0\n", "PhaseSpace:pThatMin=2.0\n", "PhaseSpace:pThatMin=3.0\n", "PhaseSpace:pThatMin=4.0\n", "PhaseSpace:pThatMin=5.0\n", "PhaseSpace:pThatMin=6.0\n", "PhaseSpace:pThatMin=7.0\n", "PhaseSpace:pThatMin=8.0\n", "PhaseSpace:pThatMin=9.0\n", "PhaseSpace:pThatMin=10.0\n" ]]"`
- `analysis_nrepeats = 10`
- `analysis_nevents = 1000000`
- `analysis_analysis = "ZEUS_2012_I1116258"`

Notes: this run got cancelled... Trying to make it work though. Increased the wall-time and sent to long queue, hopefully it works...

### ZEUS_2021_I869927

This one is quite different. It actually focuses on low pT events, so we could use a `pThatmin = 0` cut? We should test it out. But the only cut the paper really mentions is:

- `0.1 < pT < 5 GeV`
- `Q2 < 1 GeV2`
- Based on previous W_gamma_p ranges from the other two ZEUS analyses, I don't really have a fixed range, so I guess I could vary this to make it a little more efficient since the default cut might be too inclusive.

After discussions with Dr Ilkka Helenius, I should SoftQCD as pThatmin is applied only for hardQCD and the minimum cut that this can really be is 0.5 GeV due to `pthatmindiverge` stepping in if the `pthatmin` cut is too low (will talk about the results for other ones later and the use of QCD)

Hence, these are the settings for `ZEUS_2021_CE_wmin1`:

- `analysis_set_name = "ZEUS_2021_CE_wmin1"`
- `analysis_parameters_set = ["\n"]`
- `analysis_phase_space_set = ["Photon:Q2max=1\n", ["Photon:Wmin=10\n", "Photon:Wmin=50\n", "Photon:Wmin=50\n", "Photon:Wmin=100\n"]"`
- `analysis_nrepeats = 10`
- `analysis_nevents = 1000000`
- `analysis_analysis = "ZEUS_2012_I1116258"`


## Results

### L3_2004_I661114

Looking at the only plot available for this, the cuts appear to affect the lowest bin. We see no change in `pThatmin` from 0.0 to 1.0 which is because of the `pthatmindiverge` that is default set at 1.0 and is used to avoid pt_min cuts that are too low and obtain large cross sections.

The first step (`pthatmin=` 1.0 to 1.5 GeV) after that reduces the cross section from ~310 to ~270 pb/GeV (see the output log file and include uncertainties in these comparisons) in the lowest bin. It keeps reducing thereafter, hence no constant.

There is an issue with that initial convergence as it's all technically the same setting. There is also the issue that we don't know if `pthatmin=1.0` is too low as at very low pT the hardQCD cross sections diverge. Need to compare with the regularised set from softqcd (this won't have any cuts) to see if it is indeed too low. 

There is a final issue with the fact that the MC plots doesn't seem coherent with the data (about a factor of 2 times greater for most bins except the last, where it is around 2 times less). Although the OPAL_2008 paper mentioned that the L3 data wasn't compatible with that OPAL analysis (particularly at high pT where it is thought to be dominated by background). This does not mention the overestimation in the bins. But it has been mentioned that gamma-gamma to tau-tau has been considered as background, but has not been explicitly cut in the analysis which may mean that we are producing background in the MC that has been cut in the data. A solution to account for this is to use:
- `photoncollision:gmgm2qqbar = on`
- `photoncollision:gmgm2ccbar = on`
- `photoncollision:gmgm2bbbar = on`

instead of 
- `photoncollision:all = on`

### OPAL_2008_I754316

This has similar issues to the above due to the similarity in the analyses. However, it doesn't have that discrepancy with data at high pT.

The issue about softQCD and hardQCD also could be an issue. Although, a convergence is seen for `pthatmin` at 1 to 3 GeV. So, I could narrow the search there and also plot the softQCD prediction

### OPAL_2003_I611415

There's 24 plots here, a description of them is given in the `.md` file that describes all the analyses and their plots (need to write).

I have primarily looked at the `d03-x01-y01` plot which describes the cross section against Et for all x_gamma. Which clearly shows an MC estimate by over a factor of 2. The lowest bin also converges between `pthatmin` values 1 GeV and 3 GeV. But again, a softQCD comparison may be due. 


### Strategy to deal with these LEP issues**

I will probe a narrower range of the `pthatmin` values around areas that might be converging for the new `photoncollision` settings.

I will then switch on `SoftQCD:nondiffractive` events for these instead of HardQCD to verify whether the low pT cross section is an inaccurate divergence 

(why that order?)

Notes: Ok so I ran submissions jobs for them but accidentally mixed around the names of the OPAL 2003 and OPAL 2008 runs (i.e., their runs should be fine, but the analysis name is what's confusing because it's for the other OPAL run)

### ZEUS_2001_S4815815

Having trouble trying to get the plots ... 

Did some of them which might suggest pthatmin of 6 GeV, but plot d12 suggests there is no convergence?

### ZEUS_2012_I1116258

Looking at the Et plots, not much consistency on where pThatmin converges ... Not sure what to choose? Because I have to justify it.

Meeting from 10 Feb 2022 agreed that I use `pthatmin=3 GeV` for them so far. 







