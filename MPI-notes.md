# PHAS0097 UE Tuning - MPI notes
Date: 20.02.22

This document is just to write about the various tuning parameters on offer and the MPI model that Pythia uses. To be honest, this should probably be incorporated into the `Tuning.md` file but we'll keep it here for now.

## Generally, Pythia:

In the Pythia6.4 manual (describes most of the physics accurately), evolution of an event is described:

- two beam particles come at each other and are characterised by PDFs
- **IS** showers built up by a shower initiator parton from each beam
- one incoming parton from each shower enters hard process to produce outgoing partons (usually two) --> **main characteristics of event determined here**
-   hard process may also produce short-lived resonances like Z0/W±
- **FS** showers built up by outgoing partons branching
- other semihard interactions between other partons may happen **MPI??**
- beam remnants with possible internal structure and net colour charge left behind after a shower initiator is taken out
- **fragmentation** of outgoing quarks and gluons (can be seen in set of separate colour singlet subsystems, but interconnection effects can complicate this)
- many produced hadrons decay further if unstable

### Beam remnants and MPI

**Example of beam remnant**, imagine hadron-hadron collision with single parton-parton interaction (ignore MPI for now).
- One shower initiator taken from each beam which takes some fraction of beam energy, leaves behind a beam remnant which takes the rest (for proton beam, a u quark might be the initiator and leave behind ud beam remnant, which is antritriplet colour charged and colour-connected to hard interaction)
- Assign **primordial transverse momentum kT** to shower initiator to take into account motion of quarks in original hadrons, and recoil is taken up by beam remnant

Two possible MPIs: one single parton scatters against several partons from other beam, or several partons from each beam take part in separate 2->2 scatterings. Latter is favoured and is the mechanism considered in Pythia.
- Dominant 2->2 QCD cross section divergent for pT->0
- Probably lowest-order perturbative cross section regularised at small pT by colour coherence (exchanged gluon with small pT has large transverse wave function and cannot resolve individual colour charges of two incoming hadrons and couples to an average colour charge that vanishes as pT->0). 
- pTmin introduced, below which perturtbative cross section assumed vanishing or strongly damped (order of 1.5-2.5 GeV, although there's some energy dependence)
- 

### Basic MPI philosophy

**Old model**

Total rate of parton-parton interactions, as function of transverse momentum scale pT, is assumed to be given by pQCD. True for large pT, but extention of perturbative framework into low pT requires regularisation of the cross section for pT -> 0. (gives main free parameter of the model)

Assuming different parton-parton interactions are independent, the number of interactions given by poisson distribution ('simple' scenario).

 BUT hadrons are also extended objects, so collisions can be very central or peripheral (less MPI in latter), gives different poission distributions and would widen the distribution in number of interactions. This is dependent on the assumed matter distribution inside -> 'complex' scenario. 



## MPI in Pythia

### Matching to hard process
Max pT to be allowed for MPI?
**MultipartonInteractions:pTmaxMatch (default=0, min=0, max=3)**
- How maximum scale for MPI is set to match hard process scale
- option = 0: **i)** if final state contains only quarks and gluons and photons then pT\_max is factorisation scale **ii)** if not, then MPI go all the way up to kinematical limit

### Cross-section parameters
Determine the rate of interactions

**MultipartonInteractions:alphaSvalue (default = 0.130, min = 0.06, max = 0.25)**

MultipartonInteractions:alphaSorder (default = 1; minimum = 0; maximum = 2)

MultipartonInteractions:alphaEMorder (default = 1; minimum = -1; maximum = 1)

MultipartonInteractions:Kfactor (default = 1.0; minimum = 0.5; maximum = 4.0)

MultipartonInteractions:processLevel (default = 3; minimum = 0; maximum = 3)

### Cross-section regularisation
Two ways to regularise the small-pT divergence: sharp cutoff or smooth dampening. 

MultipartonInteractions:pT0parametrization (default = 0; minimum = 0; maximum = 1)
- Power law dependence on ecmNow (default for hadron beams)
- Logarithmic dependence on ecmNow (default for photon-photon collisions)

MultipartonInteractions:pT0Ref (default = 2.28; minimum = 0.5; maximum = 10.0)
- A key parameter in a Pythia tune

MultipartonInteractions:ecmRef (default = 7000.0; minimum = 1.)

MultipartonInteractions:ecmPow (default = 0.215; minimum = 0.0; maximum = 0.5)

MultipartonInteractions:pTmin (default = 0.2; minimum = 0.1; maximum = 10.0)

MultipartonInteractions:enhanceScreening (default = 0; minimum = 0; maximum = 2)


### Impact-parameter dependence

MultipartonInteractions:bProfile (default = 3; minimum = 0; maximum = 4)

MultipartonInteractions:coreRadius (default = 0.4; minimum = 0.1; maximum = 1.)

MultipartonInteractions:coreFraction (default = 0.5; minimum = 0.; maximum = 1.)

MultipartonInteractions:expPow (default = 1.85; minimum = 0.4; maximum = 10.)

MultipartonInteractions:a1 (default = 0.15; minimum = 0.; maximum = 2.)

MultipartonInteractions:bSelScale (default = 1; minimum = 1; maximum = 3)
- sort of negligible?
