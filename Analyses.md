# PHAS0097 UE Tuning - Analyses Notes
Date: 20.02.22

This document is going to be about the analyses. I will provide a description of what their aims are and their results. More technically, I will also describe the plots produced and the regions of phase space they've looked at.

I will then try to analyse what plots have correlations and what plots would be good for cross-checking, what plots would be independent and what would be possibly sensitive to MPIs. This will allow us to review which ones will be used for comparisons in the tunings as using all of the plots seems unnecessary and tells us little.

So, starting with the ZEUS ones ... 

Also, the choice for these is the fact that they have non-diffractive photoproduction. There have been multiple other analyses that involve diffractive photoproduction but those would not be clean environments to check the contribution of MPI in photoproduction because diffraction is not well-understood and separating both UE and diffractive event influences would be hard to separate and absolve. 

## Dijet photoproduction at HERA and the structure of the photon (ZEUS_2001_S4815815)

### Description

(Information is mainly from the abstract.) 

But the analysis calculates the **dijet cross section** in ZEUS photoproduction in the **phase space**: `Q2 < 1 GeV2` and photon-proton eCM `134 < W_gamma_p < 277 GeV` and transverse energies of jets `Et_jet1 > 14 GeV` and `Et_jet2 > 11 GeV` and pseudorapidity regions `-1 < nu_jet1,2 < 2.4`.

The analysis aims to test the validity of the parton densities in the photon in the range `0.1 < x_gamma < 1` and does this by comparison with NLO QCD

### Observables

**Dijet photon-proton cross section** written as convolution of proton PDFs and photon PDFs and partonic hard cross section. 

**Dijet angle theta\*** for massless partons in parton-parton CM frame is the scattering angle given by:
cos(theta\*) = tanh( (nu_jet1 - nu_jet2)/2 )

Where nu is the pseudorapidity in the laboratory frame of the two jets of highest transverse energy. 

**Fraction of photon momentum participating in production of the two highest transverse-energy jets** x^obs\_gamma (as opposed to x\_gamma which isn't directly measurable, but is equal to it in LO QCD) as is a function of the transverse jet energies in lab frame and the the fraction of the positron's energy carried by photon in proton rest frame.

**Transverse jet energy (of highest energy jet)**

### Plots

24 plots in total, showing cross sections against
- magnitude of the **dijet angle theta\***
- Et jet 1
- nu jet2
- x obs gamma

Here's the list:
- **d01-x01-y01 and d01-x01-y02** magnitude of the **dijet angle theta\*** for x obs gamma `< 0.75` and `>0.75`
- **d02-x01-y01 and d03-x01-y01 and d04-x01-y01 and d05-x01-y01 and d06-x01-y01 and d07-x01-y01** Et jet1 for x obs gamma `> 0.75` for different regions of nu jet 1 and 2 (-1-0,-1-0; 0-1,-1-0; 0-1,0-1; 1-2.4,-1-0; 1-2.4,0-1; 1-2.4,1-2.4, respectively)
- **d08-x01-y01 and d09-x01-y01 and d10-x01-y01 and d11-x01-y01 and d12-x01-y01 and d13-x01-y01** same as above but for x obs gamma `< 0.75`
- **d14-x01-y01 and d15-x01-y01 and d16-x01-y01** nu jet2 for x obs gamma `> 0.75` at nu jet1 regions: -1-0; 0-1; 1-2.4
- **d17-x01-y01 and d18-x01-y01 and d19-x01-y01** same as above but for x obs gamma `< 0.75`
- **d20-x01-y01 and d21-x01-y01 and d22-x01-y01 and d23-x01-y01** x obs gamma (1-0) at different ETjet1 regions (14-17; 17-25; 25-35; 35-90 GeV)

### Discussions and notes

Thought process, most hollistic picture given by d20-d23 as they show x obs at different Etjet1 energies so they must be independent.





## Inclusive-jet photoproduction at HERA and determination of αs (ZEUS\_2012\_I1116258)

### Description

(Mainly from the abstract.)
ep -> e + jet + X jet cross sections measured in region Q2 `< 1 GeV2` and gamma-proton CM energy `142 < W_gamma_p < 293 GeV` at ZEUS. 

Jets identified using different algorithms in lab frame, and cross sections calculated as functions of Etjet and nujet in regions `> 17 GeV` and `-1 < nujet < 2.5`. Values of the strong coupling constant with its energy-scale dependence were found.

Jets in data selection, at the third-level of the trigger system selected events with at least one jet of Et > 10 GeV and 2.5 > nu. Although, after corrections implemeted we this becomes Etjet > 17 GeV and nujet -1-2.5 (which is what the bins in Rivet show)

### Observables

Single-differential inclusive-jet cross sections calculated as functions of:

- **jet transverse energy Etjet**
- **pseudorapidity nuJet**

Double-differential inclusive-jet cross sections (based on kT jet algorithm) are functions of:

- **jet transverse energy Etjet** albeit in different regions of nuJet

Single-differential cross sections based on different jet algorithms.

### Plots

Solely plotted cross sections as functions of Et or nuJet

- **d01-x01-y01** Et in whole range (17 - 95 GeV) for all nu (-1 - 2.5)
- **d02-x01-y01** nu in whole range (-0.75 - 2.5) for all Et (> 17 GeV)
- **d03-x01-y01** nu in whole range (-0.75 - 2.5) for Et (> 21 GeV) and using kT jet alg
- **d04-x01-y01** Et in whole range (17 - 41) for nu (-1 to 0) and using kT jet alg
- **d05-x01-y01** Et in whole range (17 - 95) for nu (0 to 1) and using kT jet alg
- **d06-x01-y01** Et in whole range (17 - 95) for nu (1 to 1.5) and using kT jet alg
- **d07-x01-y01** Et in whole range (17 - 95) for nu (1.5 to 2) and using kT jet alg
- **d08-x01-y01** Et in whole range (17 - 95) for nu (2 to 2.5) and using kT jet alg
- **d09-x01-y01** Et in whole range (17 - 95) for nu (-1 to 2) and using anti-kT jet alg
- **d10-x01-y01** Et in whole range (17 - 95) for nu (-1 to 2.5) and using SISCone jet alg
- **d11-x01-y01** nu in whole range (-0.75 - 2.5) for all Et (> 17 GeV) and anti-kT jet alg
- **d12-x01-y01** nu in whole range (-0.75 - 2.5) for all Et (> 17 GeV) and SISCone jet alg

### Discussions



